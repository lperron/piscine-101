# piscine-101

<p align='center'> Some of my favorites projects realized during my "piscine" at 101! </p>

## Sastantua

The first real project of the "piscine", it just draws a pyramid on the terminal, more complicated than it seems!

<strong> Usage : </strong>

```
~$ git clone https://gitlab.com/lperron/piscine-101.git
~$ cd Sastantua/
~$ gcc -o Sastantua sastantua.c ft_atoi.c -Wall  -Wextra
~$ ./Sastantua
```

It will generate a 8 stages pyramid.

You can also specify the number of stages if you wish.

```
~$ ./Sastantua 5
```

## BSQ 

The final project, made in three days with mtaquet. Its goal is to find the greatest perfect square in a map with obstacles.

This project uses a not-so-stupid-and-pretty-fast bruteforce algorithm to solve the problem within a reasonable time.

<strong> Usage : </strong>

```
~$ git clone https://gitlab.com/lperron/piscine-101.git
~$ cd BSQ/
~$ make
~$ ./generator 1000 1000 100 | ./bsq
```

It will generate a 1000 * 1000 map with a density of 100 and solve it.

As the generator can be much slower than bsq I suggest to use a map file instead of piping directly the generator to bsq :

```
~$ ./generator 1000 1000 100 > map.txt
~$ ./bsq ./map.txt
```

This bsq can easily handle 10000 * 10000 maps but the output will not be user-friendly, a 100 * 100 map will be easier to understand!
