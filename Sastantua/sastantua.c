/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   sastantua.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: lperron <marvin@le-101.fr>                 +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/08/04 18:16:24 by lperron      #+#   ##    ##    #+#       */
/*   Updated: 2018/08/05 21:24:28 by lperron     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */
#include <unistd.h>

int	ft_atoi(const char *nbr);

void	ft_putchar(char c)
{
    write(1,&c,1);
}

int		return_size(int n, int il, int new, int mod)
{
	int c;
	int l;
	int nbl;

	c = 3;
	l = 1;
	nbl = 3;
	while (n > 1 || il != l)
	{
		if (nbl == l)
		{
			l = 1;
			new++;
			if (new != 1 && (new + 1) % 2 == 0)
				mod++;
			c = c + 4 + mod * 2;
			nbl++;
			n--;
		}
		else
			l++;
		c += 2;
	}
	return (c);
}

void	print_line(int n, int l, int size_max)
{
	int size;
	int i;

	i = -1;
	size = return_size(n, l, 0, 0);
	while (++i < (size_max - size) / 2)
		ft_putchar(' ');
	ft_putchar('/');
	i = -1;
	while (++i < size - 2)
		ft_putchar('*');
	ft_putchar(92);
	ft_putchar('\n');
}

void	print_door(int n, int size_max, int h_door, int i)
{
    int size;
    int j;
    
    j = 0;
    size = return_size(n, i - 2, 0, 0);
    while (++j < (size_max - size) / 2)
        ft_putchar(' ');
    ft_putchar('/');
    j = -1;
    while (++j < (size - n - 1 + n % 2) / 2 + (n + 1) % 2)
        ft_putchar('*');
    j = -1;
    while (++j < n - 1 + n % 2)
    {
        if (n > 4 && j == (n - 1) + n % 2 - 2 &&
            (i - 1 == h_door / 2 + 2 + (n - 1) % 2))
            ft_putchar('$');
        else
            ft_putchar(124);
    }
    j = -1;
    while (++j < (size - ((n - 1) + n % 2)) / 2)
        ft_putchar('*');
    ft_putchar(92);
    ft_putchar('\n');
}

void	print_stage(int n, int size_max, int last_stage, int h_door)
{
	int nbl;
	int i;

	i = 0;
	nbl = 3 + (n - 1);
	if (n != last_stage)
	{
		while (++i <= nbl)
			print_line(n, i, size_max);
		return ;
	}
	else
	{
		nbl = 3 + (n - 1);
		while (i++ <= nbl - h_door)
			print_line(n, i, size_max);
		while (i++ < nbl + 1)
			print_door(n, size_max, h_door, i);
	}
}

int     main(int argc, char **argv)
{
	int n;
	int stage;
	int last_stage_nbl;
	int size_max;
	int h_door;

	if (argc >= 2)
		n = ft_atoi(argv[1]);
	else
		n = 8;
	if (n < 1)
		return (1);
	stage = 1;
	last_stage_nbl = 3 + (n - 1);
	size_max = return_size(n, last_stage_nbl, 0, 0);
	h_door = last_stage_nbl - 1 - (n + 1) % 2;
	while (stage <= n)
	{
		print_stage(stage, size_max, n, h_door);
		stage++;
	}
    return (0);
}
