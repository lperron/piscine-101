/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   utils.h                                          .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: lperron <marvin@le-101.fr>                 +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/08/20 09:03:30 by lperron      #+#   ##    ##    #+#       */
/*   Updated: 2018/08/22 12:16:57 by lperron     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#ifndef UTILS_H
# define UTILS_H

struct	s_coord
{
	int	x;
	int	y;
};

typedef	struct s_coord		t_coord;

struct	s_solution
{
	t_coord point;
	int		delta;
};

typedef struct s_solution	t_solution;

struct	s_map_info
{
	char		char_empty;
	char		char_obst;
	char		char_full;
	int			nb_obst;
	int			size_alloc;
	t_coord		size;
	t_coord		*obst;
	t_solution	sol;
};

typedef	struct s_map_info	t_map_info;

#endif
