/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   input.h                                          .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: lperron <marvin@le-101.fr>                 +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/08/20 11:50:13 by lperron      #+#   ##    ##    #+#       */
/*   Updated: 2018/08/22 14:07:40 by lperron     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#ifndef INPUT_H
# define INPUT_H
# include "utils.h"

int			ft_atoi(char *str);
int			count_obst(int id, t_map_info *map, int i, int j);
t_map_info	*read_file1(int file, int i, char c);
t_map_info	*open_file(char *file);
t_map_info	*input_obst(char *file, t_map_info *map, int x, int y);
t_coord		*realloc_arr(t_map_info *map, int new_size, int old_size);
t_map_info	*read_pipe(void);

#endif
