/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   input2.c                                         .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: mtaquet <marvin@le-101.fr>                 +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/08/20 10:32:34 by mtaquet      #+#   ##    ##    #+#       */
/*   Updated: 2018/08/22 14:39:26 by lperron     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "utils.h"
#include "input.h"
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

t_coord			*realloc_arr(t_map_info *map, int new_size, int old_size)
{
	t_coord		*new_coord;
	int			i;

	i = -1;
	if (!map)
		return (NULL);
	if (!(new_coord = malloc(sizeof(t_coord) * new_size)))
		return (NULL);
	while (++i < old_size)
	{
		new_coord[i].x = map->obst[i].x;
		new_coord[i].y = map->obst[i].y;
	}
	free(map->obst);
	return (new_coord);
}

t_map_info		*input_obst(char *file, t_map_info *map, int x, int y)
{
	char	arg;
	int		i;
	int		fd;

	i = -1;
	if (!(map->obst = malloc(sizeof(t_coord) * (map->nb_obst))))
		return (0);
	if ((fd = open(file, O_RDONLY)) == -1)
		return (0);
	while (read(fd, &arg, 1) != 0)
	{
		if (arg == map->char_obst && y > 0)
		{
			map->obst[++i].x = x;
			map->obst[i].y = y - 1;
		}
		x++;
		if (arg == '\n')
		{
			y++;
			x = 0;
		}
	}
	close(fd);
	return (map);
}

t_map_info		*read_pipe(void)
{
	t_map_info	*map;

	if (!(map = read_file1(0, -1, ' ')))
		return (0);
	if (map->size.x < 1 || map->size.y < 1)
		return (0);
	return (map);
}
