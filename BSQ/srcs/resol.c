/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   resol.c                                          .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: mtaquet <marvin@le-101.fr>                 +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/08/20 13:53:42 by mtaquet      #+#   ##    ##    #+#       */
/*   Updated: 2018/08/22 12:45:23 by mtaquet     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "utils.h"

void		update_sol(t_map_info *map, int x, int y)
{
	map->sol.delta = map->sol.delta + 1;
	map->sol.point.x = x;
	map->sol.point.y = y;
}

t_map_info	*condition(t_map_info *map, int *x, int y, int i_min)
{
	int i;

	i = i_min - 1;
	while (++i < map->nb_obst)
	{
		if (((map->obst[i].x < *x || (*x + map->sol.delta < map->obst[i].x))
			|| (map->obst[i].y < y || (y + map->sol.delta < map->obst[i].y)))
			&& (*x + map->sol.delta < map->size.x
			&& y + map->sol.delta < map->size.y))
		{
			if (i == map->nb_obst - 1)
			{
				update_sol(map, *x, y);
				i = i_min - 1;
			}
		}
		else if (map->obst[i].x >= *x && *x + map->sol.delta >= map->obst[i].x)
		{
			*x = map->obst[i].x;
			break ;
		}
		else
			break ;
	}
	return (map);
}

t_map_info	*resol(t_map_info *map, int x, int y, int i_min)
{
	map->sol.delta = 0;
	if (map->nb_obst == 0)
	{
		map->nb_obst = 1;
		map->obst[0].x = -1;
		map->obst[0].y = -1;
	}
	while (y + map->sol.delta < map->size.y)
	{
		x = 0;
		while (x < map->size.x)
		{
			map = condition(map, &x, y, i_min);
			if (map->obst[i_min].x == x && map->obst[i_min].y == y)
				i_min++;
			if (i_min == map->nb_obst)
				i_min--;
			x++;
		}
		y++;
	}
	return (map);
}
