/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   main.c                                           .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: lperron <marvin@le-101.fr>                 +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/08/20 12:41:11 by lperron      #+#   ##    ##    #+#       */
/*   Updated: 2018/08/22 20:56:05 by lperron     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include <unistd.h>
#include <stdlib.h>
#include "utils.h"
#include "input.h"
#include "output.h"
#include "resol.h"

void	delete_map(t_map_info **map)
{
	if (!(*map))
		return ;
	if ((*map)->obst)
	{
		free((*map)->obst);
		(*map)->obst = NULL;
	}
	free(*map);
	*map = NULL;
}

void	polysolver(int argc, char **argv)
{
	int			i;
	t_map_info	*map;

	i = 0;
	while (++i < argc)
	{
		if ((map = open_file(argv[i])))
		{
			map = resol(map, 0, 0, 0);
			output(map, -1, -1, 0);
			delete_map(&map);
			if (i < argc - 1 && argc > 2)
				write(1, "\n", 1);
		}
		else
		{
			write(2, "map error\n", 10);
			if (i < argc - 1 && argc > 2)
				write(2, "\n", 1);
		}
	}
}

int		main(int argc, char **argv)
{
	t_map_info *map;

	if (argc > 1)
		polysolver(argc, argv);
	else
	{
		if (!(map = read_pipe()))
		{
			write(2, "map error\n", 10);
		}
		else
		{
			map = resol(map, 0, 0, 0);
			output(map, -1, -1, 0);
			delete_map(&map);
		}
	}
}
