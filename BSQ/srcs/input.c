/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   input.c                                          .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: lperron <marvin@le-101.fr>                 +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/08/20 09:48:57 by lperron      #+#   ##    ##    #+#       */
/*   Updated: 2018/08/22 16:36:15 by mtaquet     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include <sys/types.h>
#include <sys/uio.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include "utils.h"
#include "input.h"

int				read_buf(char c, t_map_info *map, int *i, int *j)
{
	if (c == map->char_obst)
	{
		map->nb_obst++;
		if (map->nb_obst >= map->size_alloc)
		{
			map->size_alloc *= 2;
			if (!(
			map->obst = realloc_arr(map, map->size_alloc, map->size_alloc / 2)))
				return (0);
		}
		map->obst[map->nb_obst - 1].x = *j - 1;
		map->obst[map->nb_obst - 1].y = *i;
	}
	else if (c == '\n')
	{
		(*i)++;
		if (map->size.x == 0)
			map->size.x = *j - 1;
		else if (map->size.x != *j - 1)
			return (0);
		*j = 0;
	}
	else if (c != map->char_empty)
		return (0);
	return (1);
}

int				count_obst(int id, t_map_info *map, int i, int j)
{
	char	c[4096];
	int		k;
	int		buffsize;

	map->size_alloc = 4096;
	map->size.x = 0;
	map->nb_obst = 0;
	if (!(map->obst = malloc(sizeof(t_coord) * map->size_alloc)))
		return (0);
	while ((buffsize = read(id, c, 4096)))
	{
		k = -1;
		while (++k < buffsize)
		{
			j++;
			if (c[k] == '\0')
				break ;
			if (!(read_buf(c[k], map, &i, &j)))
				return (0);
		}
	}
	close(id);
	if (i != map->size.y)
		return (0);
	return (1);
}

int				first_line(char *str, t_map_info *map, int size)
{
	int		i;
	char	*substr;

	i = -1;
	while (str[++i] != '\n' && i < 31)
		size++;
	if (!(substr = malloc(sizeof(char) * (size - 3))))
		return (0);
	map->char_empty = str[size - 3];
	map->char_obst = str[size - 2];
	map->char_full = str[size - 1];
	i = -1;
	while (++i < size - 3)
	{
		if (!(str[i] <= '9' && str[i] >= '0'))
		{
			free(substr);
			return (0);
		}
		substr[i] = str[i];
	}
	map->size.y = ft_atoi(substr);
	free(substr);
	return (1);
}

t_map_info		*read_file1(int file, int i, char c)
{
	t_map_info	*map;
	char		line1[32];

	if (!(map = malloc(sizeof(t_map_info))))
		return (0);
	while (read(file, &c, 1))
	{
		line1[++i] = c;
		if (line1[i] == '\n' || i == 31)
			break ;
	}
	if (!(first_line(line1, map, 0)))
		return (0);
	if (!(count_obst(file, map, 0, 0)))
		return (0);
	return (map);
}

t_map_info		*open_file(char *file)
{
	int				id;
	t_map_info		*map;

	id = open(file, O_RDONLY);
	if (id == -1)
		return (0);
	if (!(map = read_file1(id, -1, ' ')))
		return (0);
	if (map->size.x < 1 || map->size.y < 1)
		return (0);
	if (map->char_obst == map->char_empty || map->char_empty == map->char_full)
		return (0);
	return (map);
}
