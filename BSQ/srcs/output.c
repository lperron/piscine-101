/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   output.c                                         .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: mtaquet <marvin@le-101.fr>                 +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/08/20 09:50:39 by mtaquet      #+#   ##    ##    #+#       */
/*   Updated: 2018/08/22 12:24:34 by mtaquet     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "utils.h"
#include <unistd.h>
#include <stdlib.h>

void	output(t_map_info *map, int x, int y, int i)
{
	char *line;

	if (!(line = malloc(sizeof(char) * map->size.x + 1)))
		return ;
	while (++y < map->size.y)
	{
		x = -1;
		while (++x < map->size.x)
		{
			if (map->obst[i].x == x && map->obst[i].y == y)
			{
				line[x] = map->char_obst;
				i++;
			}
			else if (x >= map->sol.point.x
				&& x < map->sol.point.x + map->sol.delta &&
				y >= map->sol.point.y && y < map->sol.point.y + map->sol.delta)
				line[x] = map->char_full;
			else
				line[x] = map->char_empty;
		}
		line[x] = '\n';
		write(1, line, map->size.x + 1);
	}
	free(line);
}
